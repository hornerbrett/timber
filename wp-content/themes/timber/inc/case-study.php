<?php

define('WP_USE_THEMES', false);
require('../../../../wp-blog-header.php');
header("HTTP/1.1 200 OK");

if(isset($_GET['id'])) {
   $id = $_GET['id'];
}

$temp = $wp_query;
$wp_query = null;
$wp_query = new WP_Query();


$args = array(
  'post_type' => 'work',
  'posts_per_page' => 1,
  'p' => $id
);
$query = new WP_Query($args);

?>

<?php while($query->have_posts()) : $query->the_post() ?>
<?php $image = get_field('image');?>

<div class="inside <?php if(get_field('gallery')) { echo 'with-gallery'; }?>">
	
	
	<?php 
	$images = get_field('gallery');
	if( $images ): ?>
	<div class="slideshow">
		<div class="flexslider">
			<ul class="slides">
		        <?php foreach( $images as $image ): ?>
		            <li>
		                <img src="<?php echo $image['url']; ?>">
		            </li>
		        <?php endforeach; ?>
		    </ul>    
		</div>
	</div>
	<?php endif; ?>
					
	<div class="text" id="info<?php echo $i; ?>">	
    	<div class="container small-container content">
	    	<h3 class="color-gray">Our Client: <?php the_field('client');?></h3>	
            <h2 class="color-tan cs-title"><?php the_title();?></h2>
			<?php the_content();?>
    	</div>	
    </div>
</div>

<?php endwhile ?>

<script>
	
</script>	