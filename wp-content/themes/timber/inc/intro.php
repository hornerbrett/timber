<div class="intro">
	<div class="image-bg"></div>	
	<?php 
	if(get_field('banner_image') {
		$image = get_field('banner_image');
		$image = $image['url'];
		
	} else {
		$image = get_bloginfo('template_url') . '/img/hands.jpg';
	}
	?>
	<div class="image-fill cover" style="background-image: url(<?php echo $image;?>);"></div>
	<div class="page-text">
		<h1><?php the_field('banner_title');?></h1>
	</div>	
</div>