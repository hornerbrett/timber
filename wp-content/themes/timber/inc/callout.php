<?php if(get_field('bottom_text')) { ?>
<?php if(get_field('bottom_image')) {
	$image = get_field('bottom_image');
	$image = $image['url']; 
} else {
	$image = get_bloginfo('template_url') . '/img/landscape.jpg';
}
?>
<div class="callout landscape text-center larger" style="background-image:url(<?php echo $image;?>);">
	<div class="callout-text text-center">
		<h2 class="smaller"><?php the_field('bottom_text');?></h2>
		<?php if(get_field('bottom_button_text')) { ?>	
			<a class="btn btn-white inline-block" href="<?php the_field('bottom_button_link');?>"><?php the_field('bottom_button_text');?></a>	
		<?php } ?>
	</div>	
</div>	
<?php } ?>