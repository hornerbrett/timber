<?php
/*
 * Template Name: Default Page
 * Description: A page template with a default design.
 */
?>

<?php get_header(); ?>

<?php 
if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div id="wrapper">
	<div class="intro">
		<div class="image-bg"></div>	
		<?php $image = get_field('banner_image'); ?>
		<div class="image-fill cover" style="background-image: url(<?php echo $image['url'];?>);"></div>
		<div class="page-text">
			<h1><?php the_field('banner_title');?></h1>
		</div>	
	</div>	
	
	<div class="container smaller">
		<h1 class="color-tan"><?php the_title();?></h1>	
		<div class="content">
			<?php the_content();?>
		</div>	
	</div>	

	<?php if(get_field('bottom_text')) { ?>
	<?php if(get_field('bottom_image')) {
		$image = get_field('bottom_image');
		$image = $image['url']; 
	} else {
		$image = get_bloginfo('template_url') . '/img/landscape.jpg';
	}
	?>
	<div class="callout landscape text-center larger" style="background-image:url(<?php echo $image;?>);">
		<div class="black-bg"></div>
		<div class="callout-text text-center">
			<h2 class="smaller"><?php the_field('bottom_text');?></h2>
			<?php if(get_field('bottom_button_text')) { ?>	
				<a class="btn btn-white inline-block" href="<?php the_field('bottom_button_link');?>"><?php the_field('bottom_button_text');?></a>	
			<?php } ?>
		</div>	
	</div>	
	<?php } ?>

</div>	

<?php endwhile; 
	endif; ?>

<?php get_footer(); ?>