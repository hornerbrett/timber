<?php
/*
 * Template Name: Services Page
 * Description: A page template with a default design.
 */
?>

<?php get_header(); ?>

<div id="wrapper">
	<div class="intro">
		<div class="image-bg"></div>	
		<?php $image = get_field('banner_image'); ?>
		<div class="image-fill cover" style="background-image: url(<?php echo $image['url'];?>);"></div>
		<div class="page-text">
			<h1><?php the_field('banner_title');?></h1>
		</div>	
	</div>	
	
	<div class="callout terrain white-terrain text-center larger">
		<div class="callout-text">
			<p class="definition"><?php the_field('intro_text');?></p>	
		</div>	
	</div>	
	
	<div class="three-col">
		<div class="col width-33 pull-left bg-tan compass-col not-hovered text-center">
			<div class="inner">
				<h2><?php the_field('block_1_title');?></h2>	
				<div class="col-icon">
					<span class="arrow"><?php include get_template_directory() . '/img/icons/arrow.svg'; ?></span>	
				</div>
				<p><?php the_field('block_1_text');?></p>		
			</div>	
		</div>	
		<div class="col width-33 pull-left bg-green text-center fire-col">
			<div class="inner">
				<h2><?php the_field('block_2_title');?></h2>	
				<div class="col-icon">
					<span class="arrow"><?php include get_template_directory() . '/img/icons/fire.svg'; ?></span>	
				</div>
				<p><?php the_field('block_2_text');?></p>		
			</div>	
		</div>	
		<div class="col width-33 pull-left bg-gray text-center mountain-col">
			<div class="inner">
				<h2><?php the_field('block_3_title');?></h2>	
				<div class="col-icon">
					<span class="arrow"><?php include get_template_directory() . '/img/icons/mountain.svg'; ?></span>	
				</div>
				<p><?php the_field('block_3_text');?></p>		
			</div>	
		</div>	
	</div>	
	
	<section id="experience" class="full-float">
		<div class="container">
			<div class="image-text">
				<div class="text-side width-35 pull-left">
					<div class="text-inner content">
						<p class="topper"><?php the_field('tag');?></p>
						<h2 class="color-tan "><?php the_field('title');?></h2>
						<?php the_field('text');?>	
					</div>	
				</div>
				
				<div class="image-side width-65 pull-left">
					<div class="slides-container">
						<div class="quote-slider slider1 flexslider">
							<ul class="slides">
								
								<?php

								// check if the repeater field has rows of data
								if( have_rows('quotes_1') ):
								
								 	// loop through the rows of data
								    while ( have_rows('quotes_1') ) : the_row();
								    $image = get_sub_field('image');
								?>
								    <li>
										<div class="cover" style="background-image: url(<?php echo $image['url'];?>);">
											<div class="bg">
												<div class="quote center">
													<p class="text-left"><?php the_sub_field('quote'); ?></p>
													<br>
													<p class="from text-right"><?php the_sub_field('quoted_by'); ?></p>	
												</div>
											</div>
										</div>	
									</li>
								<?php
								    endwhile;
								
								else :
								
								    // no rows found
								
								endif;
								
								?>
								
							</ul>	
						</div>	
						
						<div class="quote-slider slider2 flexslider">
							<ul class="slides">
							<?php

								// check if the repeater field has rows of data
								if( have_rows('quotes_2') ):
								
								 	// loop through the rows of data
								    while ( have_rows('quotes_2') ) : the_row();
								    $image = get_sub_field('image_2');
								?>
								    <li>
										<div class="cover" style="background-image: url(<?php echo $image['url'];?>);">
											<div class="bg">
												<div class="quote center">
													<p class="text-left"><?php the_sub_field('quote'); ?></p>
													<br>
													<p class="from text-right"><?php the_sub_field('quoted_by'); ?></p>	
												</div>
											</div>
										</div>	
									</li>
								<?php
								    endwhile;
								
								else :
								
								    // no rows found
								
								endif;
								
								?>
							</ul>	
						</div>	
						
						<div class="quote-slider slider3 flexslider">
							<ul class="slides">
							<?php

								// check if the repeater field has rows of data
								if( have_rows('quotes_3') ):
								
								 	// loop through the rows of data
								    while ( have_rows('quotes_3') ) : the_row();
								    $image = get_sub_field('image_3');
								?>
								    <li>
										<div class="cover" style="background-image: url(<?php echo $image['url'];?>);">
											<div class="bg">
												<div class="quote center">
													<p class="text-left"><?php the_sub_field('quote'); ?></p>
													<br>
													<p class="from text-right"><?php the_sub_field('quoted_by'); ?></p>	
												</div>
											</div>
										</div>	
									</li>
								<?php
								    endwhile;
								
								else :
								
								    // no rows found
								
								endif;
								
								?>
							</ul>	
						</div>	
						
						<div class="quote-slider slider4 flexslider">
							<ul class="slides">
							<?php

								// check if the repeater field has rows of data
								if( have_rows('quotes_4') ):
								
								 	// loop through the rows of data
								    while ( have_rows('quotes_4') ) : the_row();
								    $image = get_sub_field('image');
								?>
								    <li>
										<div class="cover" style="background-image: url(<?php echo $image['url'];?>);">
											<div class="bg">
												<div class="quote center">
													<p class="text-left"><?php the_sub_field('quote'); ?></p>
													<br>
													<p class="from text-right"><?php the_sub_field('quoted_by'); ?></p>	
												</div>
											</div>
										</div>	
									</li>
								<?php
								    endwhile;
								
								else :
								
								    // no rows found
								
								endif;
								
								?>
							</ul>	
						</div>	
					</div>	
					
					
				</div>	
			</div>	
		</div>	
	</section>	
	
	<section id="specialty" class="full-float">
		<div class="container ">
			<h2 class="text-center color-tan">Areas of Specialty</h2>	
			<div class="width-100 pull-left">
				<div class="listed">
					<?php the_field('left_column');?>	
				</div>	
				<div class="listed column-2">
					<?php the_field('right_column');?>
				</div>	
			</div>	
			<div class="text-center margin-top full-float">
				<a class="btn inline-block" href="#" id="view-more">View More</a>	
			</div>	
		</div>	
	</section>	
	
	<div class="padding full-float outer">
		<section id="approach" class="bg-gray full-float">
			<div class="container smaller content">
				<div class="padding">
					<p class="topper color-white no-margin"><?php the_field('approach_tag');?></p>	
					<h2 class="color-tan"><?php the_field('approach_title');?></h2>	
					<div class="color-white">
						<?php the_field('approach_text');?>
					</div>	
				</div>	
				
				<div class="padding">
					
					<?php

					// check if the repeater field has rows of data
					if( have_rows('approaches') ):
						$i = 1;
						$rows = get_field('approaches');
					 	// loop through the rows of data
					    $row1 = $rows[0];
					    $row2 = $rows[1];
					    $row3 = $rows[2];
					?>
					    <div class="approach color-white">
							<div class="number"><?php echo $i;?></div>	
							<h2 class="color-tan uppercase"><?php echo $row1['title']; ?></h2>	
							<?php echo $row1['text']; ?>	
							<div class="stroke1">
								<?php include get_template_directory() . '/img/stroke1.svg'; ?>
							</div>	
						</div>
						<?php $i++; ?>
						
						<div class="approach color-white">
							<div class="number"><?php echo $i;?></div>	
							<h2 class="color-tan uppercase"><?php echo $row2['title']; ?></h2>	
							<?php echo $row2['text']; ?>
						</div>
						<?php $i++; ?>
						
						<div class="approach color-white">
							<div class="number"><?php echo $i;?></div>	
							<h2 class="color-tan uppercase"><?php echo $row3['title']; ?></h2>	
							<?php echo $row3['text']; ?>	
							<div class="stroke2">
								<?php include get_template_directory() . '/img/stroke2.svg'; ?>
							</div>	
						</div>
						<?php $i++; ?>
						
					<?php
					
					else :
					
					    // no rows found
					
					endif;
					
					?>
					
				</div>	
				
			</div>	
		</section>	
	</div>	
	
	<?php if(get_field('bottom_text')) { ?>
	<?php if(get_field('bottom_image')) {
		$image = get_field('bottom_image');
		$image = $image['url']; 
	} else {
		$image = get_bloginfo('template_url') . '/img/landscape.jpg';
	}
	?>
	<div class="callout landscape text-center larger" style="background-image:url(<?php echo $image;?>);" style="margin-bottom: 0;">
		<div class="black-bg"></div>
		<div class="callout-text text-center">
			<h2 class="smaller"><?php the_field('bottom_text');?></h2>
			<?php if(get_field('bottom_button_text')) { ?>	
				<a class="btn btn-white inline-block" href="<?php the_field('bottom_button_link');?>"><?php the_field('bottom_button_text');?></a>	
			<?php } ?>
		</div>	
	</div>	
	<?php } ?>

<?php get_footer(); ?>

<script type="text/javascript">
	$(window).load(function(){
		compass();
		$('.slider1').flexslider({
			directionNav: false,
			controlNav: false,
			touch: false,
			slideshowSpeed: 4000,
		});
		$('.slider2').flexslider({
			directionNav: false,
			controlNav: false,
			touch: false,
			slideshowSpeed: 5000
		});
		$('.slider3').flexslider({
			directionNav: false,
			controlNav: false,
			touch: false,
			slideshowSpeed: 6000
		});
		$('.slider4').flexslider({
			directionNav: false,
			controlNav: false,
			touch: false,
			slideshowSpeed: 7000
		});
		$('#view-more').click(function(e){
			e.preventDefault();
			if(!$(this).hasClass('showit')) {
				$(this).text('View Less').addClass('showit');
			} else {
				$(this).text('View More').removeClass('showit');
				$('html,body').animate({
				   scrollTop: $("#specialty").offset().top - 80
				});
			}
			$('.column-2').slideToggle();
		});
	});
</script>

