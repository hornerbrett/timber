<?php
/*
 * Template Name: Careers Page
 * Description: A page template with a default design.
 */
?>

<?php get_header(); ?>

<div id="wrapper">
	<div class="intro">
		<div class="image-bg"></div>	
		<?php $image = get_field('banner_image'); ?>
		<div class="image-fill cover" style="background-image: url(<?php echo $image['url'];?>);"></div>
		<div class="page-text">
			<h1><?php the_field('banner_title');?></h1>
		</div>	
	</div>	
	
	<div class="callout terrain text-center def">
		<div class="flexslider">
			<ul class="slides">
				<li>
					<div class="callout-text">
						<h1 class="word">'tim<span class="bullet"></span>b<span class="schwa">e</span>r</h1>
						<p class="definition"><i><strong>Definition:</strong> <?php the_field('definition', '5');?></i></p>	
					</div>	
				</li>	
				<?php

				// check if the repeater field has rows of data
				if( have_rows('quotes') ):
				
				 	// loop through the rows of data
				    while ( have_rows('quotes') ) : the_row();
				?>
				<li>
					<div class="floater">
					<div class="callout-text">
						<?php the_sub_field('quote'); ?>
					</div>	
					</div>	
				</li>	
				<?php
				    endwhile;
				
				else :
				
				    // no rows found
				
				endif;
				
				?>
			</ul>	
		</div>	
	</div>	
	
	<section class="full-float">
		<div class="container">
			<div class="image-text">
				<div class="image-side width-50 pull-left">
					
					<?php 
					$images = get_field('images');
					if( $images ): ?>
					        <?php foreach( $images as $image ): ?>
						        <img src="<?php echo $image['url']; ?>">
					        <?php endforeach; ?>
					<?php endif; ?>
					
				</div>	
				<div class="text-side width-50 pull-left content">
					<div class="text-inner">
						<p class="topper"><?php the_field('careers_tag');?></p>
						<h2 class="color-tan"><?php the_field('careers_title');?></h2>
						<?php the_field('careers_text');?>
						<br>
						<p class="topper"><?php the_field('benefits_tag');?></p>
						<h2 class="color-tan"><?php the_field('benefits_title');?></h2>
						<?php the_field('benefits_text');?>
						
					</div>	
				</div>
			</div>	
		</div>	
	</section>		
		
	<div class="padding full-float outer">
		<section id="jobs" class="text-center">
			<div class="container">
				
				<div class="job-text">
					<p class="topper">JOB OPPORTUNITIES</p>
					<h2 class="color-tan">Carve your niche.</h2>
					<p>Ready to roll up your sleeves, dive in, and turn bold ideas into action?</p>
				</div>	
				
<!--
				<div id="ja-jobs-widget"></div>
				<script>
					var _jaJobsSettings = {
						key: "US1_22stjqu536cubgd2yf2x7c3uda",
						jobSearchSettings: {
							showSearchForm: true,
							searchButtonText: "Search"
						},
						applicationFormSettings: {
							useExternalApplicationForm: true,
							showExternalApplicationFormInNewWindow: false
						}
					};
				</script>
				<script src="//apps.jobadder.com/widgets/v1/jobs.min.js"></script>
-->
				
			<div id="ja-jobs-widget"></div>
			<script>
			 var _jaJobsSettings = {
			 key: "us1_a4fapvvnaegerkojarjmaqlknu",
			 applicationFormSettings: {
			   useExternalApplicationForm: true,
			   showExternalApplicationFormInNewWindow: false
			 }
			 };
			</script>
			<script src="//jobadder.com/widgets/v1/jobs.min.js"></script>				
			</div>	
		</section>	
	</div>	
	
	<?php if(get_field('bottom_text')) { ?>
	<?php if(get_field('bottom_image')) {
		$image = get_field('bottom_image');
		$image = $image['url']; 
	} else {
		$image = get_bloginfo('template_url') . '/img/landscape.jpg';
	}
	?>
	<div class="callout landscape text-center larger" style="background-image:url(<?php echo $image;?>);">
		<div class="black-bg"></div>
		<div class="callout-text text-center">
			<h2 class="smaller"><?php the_field('bottom_text');?></h2>
			<?php if(get_field('bottom_button_text')) { ?>	
				<a class="btn btn-white inline-block" href="<?php the_field('bottom_button_link');?>"><?php the_field('bottom_button_text');?></a>	
			<?php } ?>
		</div>	
	</div>	
	<?php } ?>
	
</div>	

<?php get_footer(); ?>

<script type="text/javascript">
	$(document).ready(function(){
		whoweare();
		$('.flexslider').flexslider({
			directionNav: true,
		});
	});
</script>

