<?php
/*
 * Template Name: About Page
 * Description: A page template with a default design.
 */
?>

<?php get_header(); ?>

<div id="wrapper">
	<div class="intro">
		<div class="image-bg"></div>	
		<?php $image = get_field('banner_image'); ?>
		<div class="image-fill cover" style="background-image: url(<?php echo $image['url'];?>);"></div>
		<div class="page-text">
			<h1><?php the_field('banner_title');?></h1>
		</div>	
	</div>	
	
	<div class="callout terrain text-center larger">
		<div class="callout-text">
			<p class="definition"><?php the_field('intro_text');?></p>	
		</div>	
	</div>	
	
	<section class="bg-lightgray full-float">
		<div class="container">
			<div class="image-text">
				<div class="image-side width-50 pull-left">
					
					<?php 
					$images = get_field('section_1_images');
					if( $images ): ?>
					        <?php foreach( $images as $image ): ?>
						        <img src="<?php echo $image['url']; ?>">
					        <?php endforeach; ?>
					<?php endif; ?>
					
				</div>	
				<div class="text-side width-50 pull-left content">
					<div class="text-inner">
						<p class="topper"><?php the_field('section_1_tag');?></p>
						<h2 class="color-tan"><?php the_field('section_1_title');?></h2>
						<?php the_field('section_1_text');?>
					</div>	
				</div>
			</div>	
		</div>	
	</section>	
	
	<section id="team" class="full-float big-padding">
		<div class="titles text-center">
			<p class="topper"><?php the_field('team_tag');?></p>
			<h2 class="color-tan"><?php the_field('team_title');?></h2>
			<?php the_field('team_text');?>	
		</div>
		
		<div class="container">
		
			<div class="mix-container">
				
				<?php
				$i = 1;
				// check if the repeater field has rows of data
				if( have_rows('team') ):
				
				 	// loop through the rows of data
				    while ( have_rows('team') ) : the_row();
				    $image = get_sub_field('headshot');
				?>
				
				<div class="mix person">
					<div class="mix-inside">
					    <div class="mix-img">
						   	<img src="<?php echo $image['url'];?>">
					    </div>	
						
					    <div class="mix-text">
						    <div class="bg"></div>	
						    <div class="view"><span>Read More</span></div>
							<h4 class=""><?php the_sub_field('name'); ?></h4>
							<p class="job"><?php the_sub_field('position'); ?></p>	
					    </div>	
					    
					    <div class="full-information">
				            <div class="inside">
				            	<div class="text" id="info<?php echo $i; ?>">	
					            	<div class="container small-container">
										<div class="person-title">
								            <h2 class="inline"><?php the_sub_field('name'); ?></h2> <span class="divider"></span> <p class="job"><?php the_sub_field('position'); ?></p>
										</div>	
										<?php the_sub_field('bio'); ?>
										
					            	</div>	
					            </div>
				            </div>
				        </div>
					</div>
					<?php $i++;?>
				</div>
				
				<?php
				    endwhile;
				
				else :
				
				    // no rows found
				
				endif;
				
				?>
												
			</div>
		</div>
			
	</section>	
	
	<section id="culture" class=" full-float">
		<div class="container">
			<div class="image-text">
				<div class="image-side width-50 pull-left">
					<?php 
					$images = get_field('section_3_images');
					if( $images ): ?>
					        <?php foreach( $images as $image ): ?>
						        <img src="<?php echo $image['url']; ?>">
					        <?php endforeach; ?>
					<?php endif; ?>
				</div>	
				<div class="text-side width-50 pull-left">
					<div class="text-inner content">
						<p class="topper"><?php the_field('section_3_tag');?></p>
						<h2 class="color-tan"><?php the_field('section_3_title');?></h2>
						<?php the_field('section_3_text');?>
					</div>	
				</div>
			</div>	
		</div>	
	</section>	

	
	<?php if(get_field('bottom_text')) { ?>
	<?php if(get_field('bottom_image')) {
		$image = get_field('bottom_image');
		$image = $image['url']; 
	} else {
		$image = get_bloginfo('template_url') . '/img/landscape.jpg';
	}
	?>
	<div class="callout landscape text-center larger" style="background-image:url(<?php echo $image;?>);">
		<div class="black-bg"></div>	
		<div class="callout-text text-center">
			<h2 class="smaller"><?php the_field('bottom_text');?></h2>
			<?php if(get_field('bottom_button_text')) { ?>	
				<a class="btn btn-white inline-block" href="<?php the_field('bottom_button_link');?>"><?php the_field('bottom_button_text');?></a>	
			<?php } ?>
		</div>	
	</div>	
	<?php } ?>
	
</div>	

<?php get_footer(); ?>

<script type="text/javascript">
	$(document).ready(function(){
		whoweare();
	});
</script>

