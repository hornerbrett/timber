<?php
/*
 * Template Name: Contact Page
 * Description: A page template with a default design.
 */
?>

<?php get_header(); ?>

<?php 
if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div id="wrapper">
	<div class="intro">
		<?php $image = get_field('banner_image'); ?>

		<div class="image-bg">
			<video autoplay muted loop id="introvid" poster="<?php echo $image['url'];?>">
			 	 <source src="<?php bloginfo('template_url'); ?>/videos/skyline.mp4" type="video/mp4">
			</video>
		</div>	
<!-- 		<div class="image-fill cover" style="background-image: url(<?php echo $image['url'];?>);"></div> -->
		
		<div class="page-text">
			<h1><?php the_field('banner_title');?></h1>
		</div>	
	</div>	
	
	<div class="container smaller">
		
		<h2><span class="color-tan"><?php the_field('gold_title');?></span>	
			<br><?php the_field('gray_title');?></h2>	
		
		<div class="content">
			<?php the_content();?>
		</div>	
	</div>	
	
	<?php if(get_field('bottom_text')) { ?>
	<?php if(get_field('bottom_image')) {
		$image = get_field('bottom_image');
		$image = $image['url']; 
	} else {
		$image = get_bloginfo('template_url') . '/img/landscape.jpg';
	}
	?>
	<div class="callout landscape text-center larger" style="background-image:url(<?php echo $image;?>);">
		<div class="black-bg"></div>
		<div class="callout-text text-center">
			<h2 class="smaller"><?php the_field('bottom_text');?></h2>
			<?php if(get_field('bottom_button_text')) { ?>	
				<a class="btn btn-white inline-block" href="<?php the_field('bottom_button_link');?>"><?php the_field('bottom_button_text');?></a>	
			<?php } ?>
		</div>	
	</div>	
	<?php } ?>
	
</div>	

<?php endwhile; 
	endif; ?>

<?php get_footer(); ?>