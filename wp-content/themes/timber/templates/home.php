<?php
/*
 * Template Name: Home Page
 * Description: A page template with a default design.
 */
?>

<?php get_header(); ?>

<div id="fullpage">
	<div class="section active" id="section1" data-section-name="section1">
		<div id="banner-logo" class="center text-center">
			<?php include get_template_directory() . '/img/logo.svg'; ?>
			<h1 class="color-white fadeit smaller"><?php the_field('tagline');?></h1>	
		</div>
		<div class="image-bg"></div>	
		<?php 
			$image = get_field('background');
			$image = $image['url'];
		?>
		<div class="image-fill cover" style="background-image: url(<?php echo $image;?>);"></div>	
	</div>	
	<div class="section hidden" id="section2" data-section-name="section2">
		<?php 
			$image = get_field('background_1');
			$image = $image['url'];
		?>
			<div class="image-fill cover" style="background-image: url(<?php echo $image;?>);" style="z-index: 7; background-position: right center;"></div>
			
			<div class="image-bg">
				<video data-autoplay muted loop id="myVideo" poster="<?php echo $image['url'];?>">
				 	 <source src="<?php bloginfo('template_url'); ?>/videos/home-strategicpartner.mp4" type="video/mp4">
				</video>
			</div>	
<!-- 		<div class="section-logo"><?php include get_template_directory() . '/img/logo.svg'; ?></div>	 -->
		<div class="page-text">
			<div class="type"><?php the_field('gold_title_1');?></div>	
			<h1><?php the_field('title_1');?> <br><span class="color-tan typed"></span></h1>
			<?php the_field('text_1');?>
		</div>	
	</div>	
	<div class="section hidden" id="section3" data-section-name="section3">
		<?php 
			$image = get_field('background_2');
			$image = $image['url'];
		?>
		<div class="image-fill cover" style="background-image: url(<?php echo $image;?>);"></div>
		<div class="image-bg"></div>	
<!-- 		<div class="section-logo"><?php include get_template_directory() . '/img/logo.svg'; ?></div>	 -->
		<div class="page-text">
			<div class="type"><?php the_field('gold_title_2');?></div>	
			<h1><?php the_field('title_2');?> <br><span class="color-tan typed"></span></h1>
			<?php the_field('text_2');?>
		</div>	
	</div>	
	
	<div id="normal" class="section hidden fp-auto-height-responsive fp-auto-height" style="height:2000px;" data-section-name="section4">
		<div class="normal-wrapper">	
			<div class="three-col">
				
				<div class="col width-33 pull-left bg-tan compass-col not-hovered text-center">
					<div class="inner">
						<h2><?php the_field('block_1_title');?></h2>	
						<div class="col-icon">
							<span class="arrow"><?php include get_template_directory() . '/img/icons/arrow.svg'; ?></span>	
						</div>
						<p><?php the_field('block_1_text');?></p>		
					</div>	
				</div>	
				<div class="col width-33 pull-left bg-green text-center fire-col">
					<div class="inner">
						<h2><?php the_field('block_2_title');?></h2>	
						<div class="col-icon">
							<span class=""><?php include get_template_directory() . '/img/icons/fire.svg'; ?></span>	
						</div>
						<p><?php the_field('block_2_text');?></p>		
					</div>	
				</div>	
				<div class="col width-33 pull-left bg-gray text-center mountain-col">
					<div class="inner">
						<h2><?php the_field('block_3_title');?></h2>	
						<div class="col-icon">
							<span class=""><?php include get_template_directory() . '/img/icons/mountain.svg'; ?></span>	
						</div>
						<p><?php the_field('block_3_text');?></p>		
					</div>	
				</div>	
				
				<div class="bg-brown padding full-float text-center learn-more-full">
					<div class="container smaller">
						<a class="block btn" href="<?php the_field('button_link');?>"><?php the_field('button_text');?></a>	
					</div>	
				</div>	
				
			</div>	
			
			<div class="padding full-float outer">
				<div id="our-clients" class="text-center">
					<h1 class="color-tan smaller"><?php the_field('clients_title');?></h1>
					<div class="client-logos">
						
						<?php

						// check if the repeater field has rows of data
						if( have_rows('clients') ): 
						while ( have_rows('clients') ) : the_row();
						$image = get_sub_field('logo');
						?>
						    <div class="client-logo">
								<img src="<?php echo $image['url'];?>">
							</div>
						<?php
						endwhile;						
						endif;
						
						?>
						
					</div>		
				</div>	
			</div>
			
			<div id="recent-work" class="padding">
				<div class="container">
					
					<h1 class="smaller color-tan text-center"><?php the_field('recent_work_title');?></h1>	
					
					<div class="mix-container">
						
						<?php
						
						$post_objects = get_field('recent_work');
						
						if( $post_objects ): ?>
						    <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
						        <?php setup_postdata($post); 
							        $image = get_field('image');
						        ?>
						        <a href="<?php echo site_url();?>/case-studies?case-study=<?php echo get_the_ID() ?>" data-slug="<?php echo get_the_ID() ?>" class="work">
									<div class="inner">
										<div class="work-img cover" style="background-image: url(<?php echo $image['url'];?>);">
											<div class="bg"></div>	
											<div class="view"><span>View Project</span></div>	
										</div>	
										
										<div class="work-title text-center">
											<h3><?php the_title();?></h3>	
											<p class="tag">Case Study</p>	
										</div>	
										
										<div class="full-information">
								        </div>
										
									</div>	
								</a>
						    <?php endforeach; ?>
						    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
						<?php endif; ?>
						
					</div>	
					
					<div class="text-center">
						<a class="btn inline-block" style="margin-top: 40px;" href="<?php echo site_url();?>/case-studies">More Case Studies</a>	
					</div>	
					
				</div>	
			</div>
			
			<div class="callout terrain text-center">
				<div class="callout-text">
					<h1 class="word">'tim<span class="bullet"></span>b<span class="schwa">e</span>r</h1>
					<p class="definition"><i><strong>Definition:</strong> <?php the_field('definition', '5');?></i></p>	
				</div>	
			</div>	
			
		</div>
		<?php get_footer(); ?>
	</div>	
</div>	

<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scrolloverflow.min.js"></script>

<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/fullpage.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/typeit.min.js"></script>

<script type="text/javascript">
	
	$(document).ready(function() {
		fullP();		
	});
	
	function fullP() {
		$myFullpage = new fullpage('#fullpage', {
			licenseKey: '5D213825-6A1A43DC-AC3837FC-7B6A9F80',
			fixedElements: 'header',
	        scrollingSpeed: 1000,
			normalScrollElements: '#normal',
			scrollOverflow: true,
			navigation: true,
			afterLoad: function(origin, destination, direction){								
				var txt = $('.active').find('.type').text();
				var div = $('.active').find('.typed');
				new TypeIt(div, {
				  strings: txt,
				  speed: 50,
				  autoStart: false
				});
				compass();
			},
			onLeave: function(origin, destination, direction){
				var div = $(destination.item).find('.typed').empty();
			}
		});
		$('.hidden').addClass('showit');
	}
		
</script>