<?php
/*
 * Template Name: Case Studies Page
 * Description: A page template with a default design.
 */
?>

<?php get_header(); ?>

<div id="wrapper">
	<div class="intro">
		<div class="image-bg"></div>	
		<?php $image = get_field('banner_image'); ?>
		<div class="image-fill cover" style="background-image: url(<?php echo $image['url'];?>);"></div>
		<div class="page-text">
			<h1><?php the_field('banner_title');?></h1>
		</div>	
	</div>	
	
	<div class="callout terrain text-center larger gray-terrain">
		<div class="callout-text">
			<p class="definition"><?php the_field('intro_text');?></p>	
		</div>	
	</div>	
	
	<div id="recent-work" class="padding">
		<div class="container">
			
			<h1 class="smaller color-tan text-center"><?php the_field('recent_work_title', '5');?></h1>	
			
			<div class="mix-container">
				
				<?php
						
				$post_objects = get_field('recent_work');
				
				if( $post_objects ): ?>
				    <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
				        <?php setup_postdata($post); 
					        $image = get_field('image');
				        ?>
				        <a href="#" data-slug="<?php echo get_the_ID() ?>" class="work">
							<div class="inner">
								
								<?php if(get_field('video')) { ?>
								
								<div class="work-img cover" style="background-image: url(<?php echo $image['url'];?>);">
									<div class="vidbg">
										<video autoplay muted loop class="work-vid" poster="<?php echo $image['url'];?>">
										 	 <source src="<?php bloginfo('template_url'); ?>/videos/<?php the_field('video');?>.mp4" type="video/mp4">
										</video>
									</div>	
									<div class="bg"></div>	
									<div class="view"><span>View Project</span></div>	
								</div>	
								
								<?php  } else { ?>
								
								<div class="work-img cover" style="background-image: url(<?php echo $image['url'];?>);">
									<div class="bg"></div>	
									<div class="view"><span>View Project</span></div>	
								</div>	
																
								<?php  } ?>

								<div class="work-title text-center">
									<h3><?php the_title();?></h3>	
									<p class="tag"><?php the_field('client');?></p>	
								</div>	
								
								<div class="full-information">
						        </div>
								
							</div>	
						</a>
				    <?php endforeach; ?>
				    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
				<?php endif; ?>				

			</div>	
			
		</div>	
	</div>
		
	<div class="padding full-float outer">
		<div id="our-clients" class="text-center">
			<h1 class="color-tan smaller"><?php the_field('clients_title', '5');?></h1>
			<div class="client-logos">
				
				<?php

				// check if the repeater field has rows of data
				if( have_rows('clients', '5') ): while ( have_rows('clients', '5') ) : the_row();
				$image = get_sub_field('logo');
				?>
				    <div class="client-logo">
						<img src="<?php echo $image['url'];?>">
					</div>
				<?php
				    endwhile;						
				endif;
				
				?>
				
			</div>		
		</div>	
	</div>
	
	<?php $image = get_field('bottom_image', '10'); ?>
	<div class="callout landscape text-center larger" style="background-image:url(<?php echo $image['url'];?>);">
		<div class="black-bg"></div>	
		<div class="callout-text text-center">
			<h2 class="smaller"><?php the_field('bottom_text', '10');?></h2>
			<?php if(get_field('bottom_button_text', '10')) { ?>	
				<a class="btn btn-white inline-block" href="<?php the_field('bottom_button_link', '10');?>"><?php the_field('bottom_button_text', '10');?></a>	
			<?php } ?>
		</div>	
	</div>	
	
</div>	

<?php get_footer(); ?>

<script type="text/javascript">
	$(document).ready(function(){
		caseStudies();
	});
</script>

<?php
if(isset($_GET['case-study'])) {
   $casestudy = $_GET['case-study'];
 ?>
   <script>
	   	$(document).ready(function(){
		   	$('a.work[data-slug="<?php echo $casestudy; ?>"]').trigger('click');
	   	});
   </script>	
<?php
}
?>