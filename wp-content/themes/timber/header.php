<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
	<meta charset="utf-8">
    
    <link rel='shortcut icon' type='image/x-icon' href='<?php bloginfo('template_url'); ?>/img/favicon.png' />
    <meta name="author" content="DEI Creative in Seattle, WA" />
  	<meta name="keywords" content="" />
  	<meta name="description" content="" />
  	<meta name="robots" content="all" />
    
    <meta name="viewport" content="width=device-width, initial-scale=1" />

	<title><?php wp_title('&#124;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php wp_get_archives(array('type' => 'monthly', 'format' => 'link')); ?>
	
	<script type="text/javascript">
		var theme = '<?php echo get_template_directory_uri(); ?>';
	</script>
	
	<link rel="stylesheet" href="https://use.typekit.net/rlm4wze.css">
	<script src="https://use.fontawesome.com/cdf75c20cb.js"></script>
	<?php wp_head(); ?>
	
</head>

<body <?php body_class();?>>

	<header>	
		<a id="logo" href="<?php echo site_url();?>"><?php include get_template_directory() . '/img/logo.svg'; ?></a>	
			
			<?php
				$defaults = array(
					'echo'            => true,
					'menu'			  => 'Nav',
					'menu_id'		  => 'nav'
				);
				
				wp_nav_menu( $defaults );
			?>
			
<!--
			<li><a href="#">ABOUT</a></li>
			<li><a href="#">SERVICES</a></li>
			<li><a href="#">CASE STUDIES</a></li>
			<li><a href="#">CONTACT</a></li>
			<li class="divider">|</li>
			<li><a href="#">CAREERS</a></li>	
-->
		<a id="toggle" href="#">
			<span class="top"></span>	
			<span class="middle"></span>	
			<span class="bottom"></span>	
		</a>	
	</header>	
