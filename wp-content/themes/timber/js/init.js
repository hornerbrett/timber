$(window).load(function(){
	$(".image-fill").fadeIn(600);
});

$(function(){
	
	header();
	animations();
	
// 	var s = skrollr.init();
	
	$(window).resize(function(){
		//
	}).triggerHandler('resize');
	
});	

function animations() {
	
	$('.animate').on('inview', function(event, visible) {
		if(visible) {
			$(this).addClass('in-view');
		}
	});
	
}

function header() {
	$(window).scroll(function() {
	    var height = $(window).scrollTop();
	    if(height  > 260) {
	        $('body').addClass('scrolled').removeClass('top-page');
	    } else {
	        $('body').removeClass('open').addClass('top-page');
	    }
	});

	$('a#toggle').click(function(e) {
		e.preventDefault();
		$('body').toggleClass('open');
	});
	
	$('html').click(function() {
		$('body').removeClass('open');
	});
	
	$('#nav, a#toggle').click(function(event){
	    event.stopPropagation();
	});
	
	$('html').on('touchstart', function(e) {
		$('body').removeClass('open');
	});
			
	$('#nav, a#toggle').on('touchstart',function(e) {
	    e.stopPropagation();
	});
}

function whoweare() {
		
    $('.person').click(function() {
	    
		if(!$(this).hasClass('active')) {
			var person_information = $(this).closest('.person:visible').find('.full-information').html(),						
				expanded_information = $('<div class="column full-information left width-100 open" />'),
				top_position = $(this).closest('.person:visible').position().top,
				index_position = $(this).closest('.person:visible').index('.person:visible'),
				total_people = $('#team .mix-container .person:visible').length - 1;
				
			console.log(top_position);
				
			$('#team .mix-container .person:visible').removeClass('active').addClass('non-active');
			
			$(this).removeClass('non-active').addClass('active');	
			
			expanded_information.append(person_information);
			
			$('#team .mix-container .column.full-information').removeClass('open').addClass('remove');
			
			for(var i = index_position; i <= total_people; i++) {
				if(top_position < $('#team .mix-container .person:visible').eq(i).position().top) {
					$(expanded_information).insertBefore($('#team .mix-container .person:visible').eq(i));
					break;
				}
				if(i == total_people) {
					$(expanded_information).appendTo($('#team .mix-container'));
					break;
				}
			}
			
			$('#team .mix-container .column.full-information.remove').slideUp(125, function() {
				$('#team .mix-container .column.full-information.remove').remove();
			});
			
			expanded_information.slideDown(125, function() {
				$('#team .mix-container .column.full-information').addClass('active');
				
				$('html,body').animate({
		          scrollTop: $('.full-information.active').offset().top - 80
		        }, 500);
				
			});
			
			
		} else {
			$(this).removeClass('active').addClass('no-active');
			$('#team .mix-container .column.full-information').removeClass('125').addClass('remove');
			$('#team .mix-container .column.full-information.remove').slideUp(125, function() {
				$('#team .mix-container .column.full-information.remove').remove();
			});
		}
		return false;
	});
	
}

function caseStudies() {
	$('.work').click(function() {
	    
		if(!$(this).hasClass('active')) {
			
			$(this).removeClass('non-active').addClass('active');
			
			data = $(this).data('slug');
			
			var result = $.param(data);
			
			console.log(data);
			
			$.ajax({
				type: "GET",
				data: {
					'id' : data,	
				},
				url: theme + "/inc/case-study.php",
				beforeSend: function() {
		           $(this).find('.full-information').html('<div class="loading" />');
		        },
				success: function(data) {
					$('.work.active').find('.full-information').html(data);
					window.history.pushState("object or string", "Title", "?" + result);
	 	        },
		        complete: function() {
		        },
				async: false
			});
			
			var person_information = $(this).closest('.work:visible').find('.full-information').html(),						
				expanded_information = $('<div class="column full-information left width-100 open"><a href="#" class="close"></a><div>'),
				top_position = $(this).closest('.work:visible').position().top,
				index_position = $(this).closest('.work:visible').index('.work:visible'),
				total_people = $('#recent-work .mix-container .work:visible').length - 1;
				
			console.log(top_position);
				
			$('#recent-work .mix-container .work:visible').removeClass('active').addClass('non-active');	
			
			expanded_information.append(person_information);
			
			$('#recent-work .mix-container .column.full-information').removeClass('open').addClass('remove');
			
			for(var i = index_position; i <= total_people; i++) {
				if(top_position < $('#recent-work .mix-container .work:visible').eq(i).position().top) {
					$(expanded_information).insertBefore($('#recent-work .mix-container .work:visible').eq(i));
					break;
				}
				if(i == total_people) {
					$(expanded_information).appendTo($('#recent-work .mix-container'));
					break;
				}
			}
			
			$('#recent-work .mix-container .column.full-information.remove').slideUp(125, function() {
				$('#recent-work .mix-container .column.full-information.remove').remove();
			});
			expanded_information.slideDown(125, function() {
				$('#recent-work .mix-container .column.full-information').addClass('active');
						
				$('html, body').animate({
		          scrollTop: $('.full-information.active').offset().top - 80
		        }, 500);
				
			});
			$('.flexslider').flexslider({
				directionNav: false,
//				smoothHeight: true
			});
		} else {
			$(this).removeClass('active').addClass('no-active');
			$('#recent-work .mix-container .column.full-information').removeClass('125').addClass('remove');
			$('#recent-work .mix-container .column.full-information.remove').slideUp(125, function() {
				$('#recent-work .mix-container .column.full-information.remove').remove();
			});
		}
	
		$('a.close').click(function(e) {
			console.log('closed');
			e.preventDefault();
			$('.full-information.active').removeClass('active').addClass('no-active');
			$('#recent-work .mix-container .column.full-information').removeClass('125').addClass('remove');
			$('#recent-work .mix-container .column.full-information.remove').slideUp(125, function() {
				$('#recent-work .mix-container .column.full-information.remove').remove();
			});
		});	
	
		fullpage_api.reBuild();
	
		return false;
	});
}

function compass() {
	var img = $('.arrow');
	if(img.length > 0){
	    var offset = img.offset();
	    function mouse(evt){
	        var center_x = (offset.left) + (img.width()/2);
	        var center_y = (offset.top) + (img.height()/2);
	        var mouse_x = evt.pageX; var mouse_y = evt.pageY;
	        var radians = Math.atan2(mouse_x - center_x, mouse_y - center_y);
	        var degree = (radians * (180 / Math.PI) * -1) + 90; 
	        img.css('-moz-transform', 'rotate('+degree+'deg)');
	        img.css('-webkit-transform', 'rotate('+degree+'deg)');
	        img.css('-o-transform', 'rotate('+degree+'deg)');
	        img.css('-ms-transform', 'rotate('+degree+'deg)');
	    }
	    $('.compass-col').mousemove(mouse);
	}
	
	$('.compass-col').hover(
       function(){ 
	       $(this).removeClass('not-hovered');
	    },
       function(){ 
	       $(this).addClass('not-hovered');
	       $('.arrow').removeAttr("style")
       }
	)		
}
