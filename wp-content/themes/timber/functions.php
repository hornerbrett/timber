<?php

/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/

/**
 *  Initialize Scripts + Stylesheets
 */

function my_init() {
	
	if (!is_admin()) {
		
		// jQuery
		wp_deregister_script('jquery'); 
		wp_register_script('jquery', 'https://code.jquery.com/jquery-latest.min.js'); 
		wp_enqueue_script('jquery');
		
		// Stylesheet
		wp_enqueue_style('stylesheet-main', get_bloginfo('template_url') . '/style.css', '', '', false);
		
		// Inview
		wp_enqueue_script('inview', get_bloginfo('template_url') . '/js/jquery.inview.min.js', array('jquery'), '', false);
	
		// Skrollr
		wp_enqueue_script('skrollr', get_bloginfo('template_url') . '/js/skrollr.min.js', array('jquery'), '', false);

		// flexslider
		wp_enqueue_script('flexslider', get_bloginfo('template_url') . '/js/jquery.flexslider-min.js', array('jquery'), '', false);

		// Initialize Everything
		wp_enqueue_script('initialize', get_bloginfo('template_url') . '/js/init.js', array('jquery'), '', false);
		
	} else {
		
		// Administration Stylesheet
		wp_enqueue_style('style-admin', get_bloginfo('template_url') . '/css/style-admin.css', '', '', false);
		
	}
	
}

add_action('init', 'my_init');

/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/

/**
 * Add Theme Suport: Menus
 */

add_theme_support('menus');

/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/

/**
 * Remove Main Stylesheet From Login Page
 */
 
function login_styles() {
	wp_deregister_style('stylesheet-main');
}

add_action('login_init', 'login_styles');

/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/

/**
 * Add Styles to Wysiwyg Editor 
 */

function add_editor_stylesheet() {
	add_editor_style('css/style-editor.css');
}

add_action('after_setup_theme', 'add_editor_stylesheet');

/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/

/**
 * Add Options Panels
 */

if( function_exists('acf_add_options_page')) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Options',
		'menu_title'	=> 'Options',
		'menu_slug' 	=> 'options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
		
	
}

 
/******************************************************************************************************/
/******************************************************************************************************/

/**
 * Update The WYSIWYG Editor


function my_toolbars( $toolbars ){ 
	$toolbars['Very Basic'][1] = array('bold', 'italic', 'underline', 'bullist', 'numlist', 'link', 'unlink');
	return $toolbars;
}

add_filter('acf/fields/wysiwyg/toolbars', 'my_toolbars');
 */

/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/

/**
 * Create Custom Post Type: work
 * Create Taxonomies: work Categories
 * Create Columns: work
 * Manage Columns: work
 */
  
function create_work() {
	register_post_type(
		'work',
		array(
			'labels' => array(
				'name' => __("Work"),
				'singular_name' => __("work"),
				'add_new' => __("Add New"),
				'add_new_item' => __("Add New work"),
				'edit_item' => __("Edit work"),
				'new_item' => __("New work"),
				'view_item' => __("View work"),
				'search_items' => __("Search work"),
				'not_found' => __("No work found."),
				'not_found_in_trash' => __("No work found in trash."),
				'edit' => __("Edit work"),
				'view' => __("View work")
			),
			'exclude_from_search' => true,
			'menu_icon' => 'dashicons-desktop',
			'public' => true,
			'rewrite' => array('slug' => 'work'),
			'supports' => array('title','editor','author','excerpt','comments','revisions')
		)
	);
	flush_rewrite_rules();
}

function create_work_taxonomies() {
	register_taxonomy(
		'work-categories',
		'work',
		array(
			'hierarchical' => true,
			'label' => 'Category',
			'query_var' => true,
			'rewrite' => array('slug' => 'work-categories')
		)
	);
}

function create_work_columns($columns) {
    $columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __('Name'),
		'taxonomy' => __('Taxonomy'),
		'acf-field' => __('Advanced Custom Field'),
	);
	return $columns;
}

function manage_work_columns($column, $post_id) {
	global $post;
	switch($column) {
		case 'taxonomy':
			$work = get_post_meta($post_id, 'work', true);
			if(!empty($work)) echo $work;
			break;
		case 'acf-field':
			$field = get_post_meta($post_id, 'acf-field', true);
			if(!empty($field)) echo $field;
			break;
		default : break;
	}
}

add_action('init', 'create_work');
add_action('init', 'create_work_taxonomies');
add_filter('manage_edit-work_columns', 'create_work_columns' ) ;
add_action('manage_work_posts_custom_column', 'manage_work_columns', 10, 2 );

 
  
/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/

/**
 * Move Yoast To Bottom Of Pages

 
function yoasttobottom() {
	return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');
 */

/******************************************************************************************************/
/******************************************************************************************************/
/******************************************************************************************************/

/**
 * Disable The Stupid Block Editor
 */

add_filter('use_block_editor_for_post', '__return_false', 10);

function remove_block_css(){
wp_dequeue_style( 'wp-block-library' );
}
add_action( 'wp_enqueue_scripts', 'remove_block_css', 100 );

?>
