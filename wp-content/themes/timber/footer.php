<?php wp_footer(); ?>
	<footer>
		<div class="container">
			<div class="f-logo">
				<?php include get_template_directory() . '/img/logo.svg'; ?>
				<p class="copyright">© <?php the_date('Y');?> TIMBER IT CONSULTING <span class="divider">|</span> WEB: <a class="inline" href="https://deicreative.com">DEI</a></p>	
			</div>
			<div class="f-left">
				<a href="mailto:<?php the_field('email','options');?>"><span class="fa fa-envelope"></span> <?php the_field('email','options');?></a>
				<a href="tel:<?php the_field('phone','options');?>"><span class="fa fa-phone"></span> <?php the_field('phone','options');?></a>
			</div>	
			
			<div class="f-right">
				<a class="careers" href="<?php echo site_url();?>/careers">Careers</a>	
				<div class="social">
					<?php if(get_field('facebook', 'options')) { ?><a href="<?php the_field('facebook','options');?>" class="fa fa-facebook"></a><?php } ?>
					<?php if(get_field('instagram', 'options')) { ?><a href="<?php the_field('instagram','options');?>" class="fa fa-instagram"></a><?php } ?>
					<?php if(get_field('twitter', 'options')) { ?><a href="<?php the_field('twitter','options');?>" class="fa fa-twitter"></a><?php } ?>
					<?php if(get_field('linkedin', 'options')) { ?><a href="<?php the_field('linkedin','options');?>" class="fa fa-linkedin"></a><?php } ?>
				</div>	
			</div>		
		</div>	
	</footer>	
</body>
</html>